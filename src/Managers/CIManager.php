<?php

namespace FullCycle\SplitTest\Managers;

use Carbon\Carbon;

class CIManager extends \CI_Model implements SplitTestManagerInterface {
    protected $result = null;
    
    function getActiveSplitTests() {
        //		$this->db->where('status','active');
        $this->result = $this->db->get_where('split_test_groups',['status'=>'active']);
        return $this->result->result();
    }
    
    function getActiveRandomTest() {
        if (!$this->result)
            $this->getActiveSplitTests();
        if (!$this->result)	// No split tests are active
            return null;
        $count = $this->result->num_rows();
        srand(time());
        $random = rand(0,$count-1);
        return $this->result->row($random);
    }
    
    function getUTCTimeStamp() {
        return Carbon::now()->timezone("UTC")->toDateTimeString();
    }
    
    function getSplitTest($idx) {
        $result = $this->db->get_where('split_test_groups',['id'=>$idx]);
        return $result->first_row();
    }
    
    function incrementUserCount($id) {
        if (!$id)
            return false;
        
        $dateTime=$this->getUTCTimeStamp();
        $this->db->query("update split_test_groups set user_count = user_count + 1, updated_at='{$dateTime}' where id=$id");
    }
    
    function convertMetaArray($data) {
        $decoded = json_decode($data,true);
        return $decoded;
        
    }
   
    function convertMetaBoolean($data) {
		return ($data == "true")
    }

    function convertMetaInteger($data)
		return intval($data);
    }
 
    function convertMeta($type, $value) {
        $convertMeta = "convertMeta" . ucfirst($type);
        if (method_exists($this,$convertMeta))
            $value = $this->$convertMeta($value);
        return $value;  
    }
    
    function getMeta($id) {
        $result = $this->db->get_where('split_test_groups_meta',['split_test_group_id'=>$id]);
        $meta=[];
        foreach ($result->result() as $row) {
            $meta[$row->key] = $this->convertMeta($row->type, $row->value);
        }
        return $meta;
    }
    
    public function splitTestReplaced($splitTest,$replacedBy) {
        $dateTime = $this->getUTCTimeStamp();
        return $this->db->query("update member_split_test_group set replaced_by=$replacedBy, updated_at='{$dateTime}' 
            where split_test_group_id={$splitTest->id} and member_id={$splitTest->member_id}");
    }
    
    function addMemberToSplitTest($split_test_id, $member_id, $session_id) {
        // Don't insert the member again for the same split test, delete them first then insert
        $this->db->delete('member_split_test_group',[
            'split_test_group_id' => $split_test_id,
            'member_id' => $member_id,
            'replaced_by' => null,
            
        ]);
        $this->db->insert('member_split_test_group',[
            'split_test_group_id' => $split_test_id,
            'member_id' => $member_id,
            'session_id' => $session_id,
            'created_at' => $this->getUTCTimeStamp(),
            'updated_at' => $this->getUTCTimeStamp(),
        ]);
        $this->updateLineItem($session_id,['user_id'=>$member_id]);
    }
    
    function getForceSplitTestId() {
        return $this->input->get('force_sv_id');
    }
        
    function getMemberSplitTest($member_id) {
        if (!$member_id)
            return false;
        $result = $this->db->query("SELECT stg.*, mstg.member_id as member_id, mstg.session_id as session_id
            from member_split_test_group mstg
            join split_test_groups stg on stg.id = mstg.split_test_group_id
            where member_id=$member_id
            and status in ('active','testing') and mstg.replaced_by is null
        ");
        return $result->last_row();
    }
    
    
    function getMemberActiveSplitTest($member_id) {
        if (!$member_id)
            return false;
        $result = $this->db->query("SELECT stg.*, mstg.member_id as member_id, mstg.session_id as session_id
            from member_split_test_group mstg
            join split_test_groups stg on stg.id = mstg.split_test_group_id
            and status='active'
        ");
            return $result->last_row();
    }
    
    public function addLineItem($sv_session, $variant, $mobile = 0){
        $uri = isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:null;
        $referer = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:null;
        $useragent = isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:null;
        
        $detect = new \MobileDetect;
        $is_mobile = $detect->isMobile();
        $data = array(
            'new_account' => false,
            'mobile' => $mobile,
            'variant' => $variant,
            'revenue' => 0,
            'created_at' => $this->getUTCTimeStamp(),
            'updated_at' => $this->getUTCTimeStamp(),
            'session_id' => $sv_session,
            'uri' => $uri,
            'referer' => $referer,
            'mobile' => $is_mobile,
            'useragent' => $useragent,
            'already_logged_in' => $is_already_logged_in,
        );
        $this->db->insert('split_test', $data);
    }
    
    public function insertLineItem($sv_session,$data) {
        $data['updated_at'] = $this->getUTCTimeStamp();
        $data['created_at'] = $this->getUTCTimeStamp();
        $this->db->insert('split_test', $data);
        
    }
    
    public function updateLineItem($sv_session, $data) {
        $data['updated_at'] = $this->getUTCTimeStamp();
        $this->db->where(['session_id' => $sv_session]);
        $this->db->update('split_test',$data);
    }
    
    public function increment($table,$column,$key_field,$key_val) {
        $dateTime = $this->getUTCTimeStamp();
        $this->db->query("update $table set $column = $column + 1, updated_at='{$dateTime}' where $key_field='$key_val'");
        
    }
    
    public function incrementCartAdd($session_id) {
        if (!$session_id)
            return false;
            
        $this->increment('split_test','cart_adds','session_id',$session_id);
    }
    public function incrementDealsViewed($session_id) {
        if (!$session_id)
            return false;
            
        $this->increment('split_test','deals_viewed','session_id',$session_id);
    }
    
    public function incrementRegistered($session_id) {
        if (!$session_id)
            return false;
            
        $this->increment('split_test','registered','session_id',$session_id);
    }
    public function incrementLogins($session_id) {
        if (!$session_id)
            return false;
        $this->increment('split_test','logins','session_id',$session_id);
    }
    public function incrementLogouts($session_id) {
        if (!$session_id)
            return false;
            $this->increment('split_test','logouts','session_id',$session_id);
    }
    

}





