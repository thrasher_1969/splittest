<?php

namespace FullCycle\SplitTest\Managers;

use App\SplitTestGroup;
use App\SplitTestGroupsMeta;
use App\SplitTest;
use App\MemberSplitTestGroup;
use \DB;
use App\SplitTestOrder;
use Detection\MobileDetect;

class LaravelManager implements SplitTestManagerInterface {
    protected $result = null;
    
    function getActiveSplitTests() {
        $this->result = SplitTestGroup::where('status','active')->get();
        return $this->result;
    }
    
    function getActiveRandomTest() {
        if (!$this->result)
            $this->result = $this->getActiveSplitTests();
        if ($this->result->count() == 0)	// No split tests are active
            return null;
        $count = $this->result->count();
        srand(time());
        $random = rand(0,$count-1);
        return $this->result[$random];
    }
    
    function getSplitTest($idx) {
        $result=SplitTestGroup::find($idx);
        return $result;
    }
    
    function incrementUserCount($id) {
        if (!$id)
            return false;
        SplitTestGroup::find($id)->increment('user_count');
    }
    
    function getMeta($id) {
        $result = SplitTestGroupsMeta::where('split_test_group_id',$id)->get();
        $meta=[];
        foreach ($result as $row) {
            $meta[$row->key] = $row->value;
        }
        return $meta;
    }
    
    public function splitTestReplaced($splitTest,$replacedBy) {
        return MemberSplitTestGroup::where('member_id',$splitTest->member_id)
            ->where('split_test_group_id',$splitTest->id)->update(['replaced_by'=>$replacedBy]);
    }
    
    function addMemberToSplitTest($split_test_id, $member_id, $session_id) {
        // Don't insert the member again for the same split test, delete them first then insert
        MemberSplitTestGroup::where('split_test_group_id',$split_test_id)
            ->where('member_id',$member_id)
            ->whereNull('replaced_by')
            ->delete();
        MemberSplitTestGroup::create([
            'split_test_group_id' => $split_test_id,
            'member_id' => $member_id,
            'session_id' => $session_id,
            
        ]);
        $this->updateLineItem($session_id,['user_id'=>$member_id]);
    }
    
    function getMemberSplitTest($member_id) {
        if (!$member_id)
            return false;
        $result = DB::select(DB::raw("SELECT stg.*, mstg.member_id as member_id, mstg.session_id as session_id
            from member_split_test_group mstg
            join split_test_groups stg on stg.id = mstg.split_test_group_id
            where member_id=$member_id
            and status='active' and mstg.replaced_by is null
    "));
        if (!empty($result))
            return $result[0];
        return false;
    }
    
    // Stub this in for now.
    function getForceSplitTestId() {
        return request()->force_sv_id;
    }
    
    function getMemberActiveSplitTest($member_id) {
        if (!$member_id)
            return false;
        $result = DB::select(DB::raw("SELECT stg.*, mstg.member_id as member_id, mstg.session_id as session_id
            from member_split_test_group mstg
            join split_test_groups stg on stg.id = mstg.split_test_group_id
            and status='active'
        "));
        return $result->last_row();
    }
    
    public function addLineItem($sv_session, $variant, $mobile = 0){
        $uri = isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:null;
        $referer = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:null;
        $useragent = isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:null;
        
        $detect = new MobileDetect;
        $is_mobile = $detect->isMobile();
        SplitTest::create([
            'new_account' => false,
            'mobile' => $mobile,
            'variant'   => $variant,
            'revenue' => 0,
            'created_at' => date('Y-m-d H:i:s'),
            'session_id' => $sv_session,
            'uri' => $uri,
            'referer' => $referer,
            'mobile' => $is_mobile,
            'useragent' => $useragent,
            
        ]);
    }
    
    public function insertLineItem($sv_session,$data) {
        return SplitTest::create($data);
    }
    
    public function updateLineItem($sv_session, $data) {
        SplitTest::where('session_id',$sv_session)->update($data);
    }
    
    
    public function increment($table,$column,$key_field,$key_val) { // Not used in laravel.
       // $this->db->query("update $table set $column = $column + 1 where $key_field='$key_val'");
        
    }
    
    public function incrementCartAdd($session_id) {
        if (!$session_id)
            return false;
        $st = SplitTest::where('session_id',$session_id)->first();
        if ($st) $st->increment("cart_adds");
    }
    public function incrementDealsViewed($session_id) {
        if (!$session_id)
            return false;
            
        $st = SplitTest::where('session_id',$session_id)->first();
        if ($st) $st->increment("deals_viewed");
    }
    
    public function incrementRegistered($session_id) {
        if (!$session_id)
            return false;
            
        $st = SplitTest::where('session_id',$session_id)->first();
        if ($st) $st->increment("registered");
    }
    public function incrementLogins($session_id) {
        if (!$session_id)
            return false;
        $st = SplitTest::where('session_id',$session_id)->first();
        if ($st) $st->increment("logins");
    }
    
    public function incrementLogouts($session_id) {
        if (!$session_id)
            return false;
        return SplitTest::where('session_id',$session_id)->update(['logouts' => DB::raw('logouts + 1')]);
            
        $st = SplitTest::where('session_id',$session_id)->first();
        if ($st) $st->increment("logouts");
    }
    
    public function addOrderInfo($session_id, $order_id,$amount,$shipping,$tax,$qty) {
        if (!$session_id)
            return false;
        $splitTest = SplitTest::where('session_id',$session_id)->first();
        if (!$splitTest)
            return false;
        $splitTest->total_order_amount += $amount;
        $splitTest->total_shipping_amount += $shipping;
        $splitTest->total_tax_amount += $tax;
        $splitTest->total_items += $qty;
        $splitTest->total_orders++;
        $splitTest->order_id = $order_id;
        $splitTest->revenue = $splitTest->total_order_amount - ($splitTest->total_shipping_amount + $splitTest->total_tax_amount);
        $splitTest->save();
        SplitTestOrder::create([
            'session_id' => $session_id,
            'qty' => $qty,
            'order_id' => $order_id,
            'total_order_amount' => $amount,
            'total_shipping_amount' => $shipping,
            'total_tax_amount' => $tax,
            'split_test_group_id' => $splitTest->variant,
            'member_id' => $splitTest->user_id,
        ]);
    }
}





