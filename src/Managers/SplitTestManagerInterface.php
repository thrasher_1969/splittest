<?php

namespace FullCycle\SplitTest\Managers;

interface SplitTestManagerInterface {
    function getActiveSplitTests();
    function getActiveRandomTest();
    function getSplitTest($idx);
    function incrementUserCount($id);
    function getMeta($id);
    function addMemberToSplitTest($split_test_id, $member_id, $session_id);
    function getMemberSplitTest($member_id);
    function getMemberActiveSplitTest($member_id);
    public function addLineItem($sv_session, $variant, $mobile = 0);
    //public function increment($table,$column,$key_field,$key_val);
    public function incrementCartAdd($session_id);
    public function incrementDealsViewed($session_id);
    public function incrementRegistered($session_id);
    public function incrementLogins($session_id);
    public function updateLineItem($sv_session, $data);
    public function splitTestReplaced($splitTest,$replacedBy);
    public function insertLineItem($sv_session,$data);
    public function incrementLogouts($session_id);
        
        
}

