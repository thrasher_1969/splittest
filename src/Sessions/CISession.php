<?php namespace FullCycle\SplitTest\Sessions;
/**
 * Part of the Sentry package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.  It is also available at
 * the following URL: http://www.opensource.org/licenses/BSD-3-Clause
 *
 * @package    SplitTest
 * @version    2.0.0
 * @author     FullCycle LLC
 * @license    BSD License (3-clause)
 * @copyright  (c) 2019 - 2019, FullCycle LLC
 * @link       http://fullcycle.io
 */

use CI_Session as Session;

class CISession implements SessionInterface {

	/**
	 * The CodeIgniter session driver.
	 *
	 * @param  CI_Session
	 */
	protected $store;

	/**
	 * The key used in the Session.
	 *
	 * @var string
	 */
	protected $key = 'fullcycle_split_test';

	/**
	 * Creates a new CodeIgniter Session driver for Sentry.
	 *
	 * @param  \CI_Session  $store
	 * @param  string  $key
	 * @return void
	 */
	public function __construct(Session $store, $key = null)
	{
		$this->store = $store;

		if (isset($key))
		{
			$this->key = $key;
		}
	}

	/**
	 * Returns the session key.
	 *
	 * @return string
	 */
	public function getKey()
	{
		return $this->key;
	}

	/**
	 * Put a value in the Split Test session.
	 *
	 * @param  mixed  $value
	 * @return void
	 */
	public function put($key,$value)
	{
		$data = $this->get();
		if (!$data)
			$data=[];
		$data[$key] = $value;
		$this->store->set_userdata($this->getkey(), $data);
	}

	/**
	 * Get the SplitTest session value.
	 *
	 * @return mixed
	 */
	public function get($key=false)
	{
		$data =  $this->store->userdata($this->getKey());
		if ($key === false)
			return $data;
		if ($data && isset($data[$key]))
			return $data[$key];
		return null;
	}

	/**
	 * Remove the SplitTest session.
	 *
	 * @return void
	 */
	public function forget()
	{
		$this->store->unset_userdata($this->getKey());
	}

        public function getRaw($key) {
                return $this->store->userdata($key);
        }

        public function putRaw($key,$value) {
                return $this->store->set_userdata($key,$value);
        }

}
