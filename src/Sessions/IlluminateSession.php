<?php 

namespace FullCycle\SplitTest\Sessions;
/**
 * Part of the Sentry package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.  It is also available at
 * the following URL: http://www.opensource.org/licenses/BSD-3-Clause
 *
 * @package    Sentry
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    BSD License (3-clause)
 * @copyright  (c) 2011 - 2013, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Illuminate\Session\Store as SessionStore;

class IlluminateSession implements SessionInterface {

	/**
	 * The key used in the Session.
	 *
	 * @var string
	 */
	protected $key = 'fullcycle_split_test';

	/**
	 * Session store object.
	 *
	 * @var \Illuminate\Session\Store
	 */
	protected $session;

	/**
	 * Creates a new Illuminate based Session driver for Sentry.
	 *
	 * @param  \Illuminate\Session\Store  $session
	 * @param  string  $key
	 * @return void
	 */
	public function __construct(SessionStore $session, $key = null)
	{
		$this->session = $session;

		if (isset($key))
		{
			$this->key = $key;
		}
	}

	/**
	 * Returns the session key.
	 *
	 * @return string
	 */
	public function getKey()
	{
		return $this->key;
	}

	/**
	 * Put a value in the Sentry session.
	 *
	 * @param  mixed  $value
	 * @return void
	 */
	public function put($key,$value)
	{
	    $data = $this->get();
	    if (!$data)
	        $data=[];
	    $data[$key] = $value;
		$this->session->put($this->getKey(), $data);
	}

	/**
	 * Get the Sentry session value.
	 *
	 * @return mixed
	 */
	public function get($key=false)
	{
	    $data = $this->session->get($this->getKey());
	    if ($key === false)
	        return $data;
	    if ($data && isset($data[$key]))
	        return $data[$key];
	    return null;
	}

	/**
	 * Remove the Sentry session.
	 *
	 * @return void
	 */
	public function forget()
	{
		$this->session->forget($this->getKey());
	}

	public function getRaw($key) {
		return $this->session->get($key);
	}

	public function putRaw($key,$value) {
		return $this->session->put($key,$value);
	}
}
