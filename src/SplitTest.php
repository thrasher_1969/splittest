<?php
namespace FullCycle\SplitTest;

use FullCycle\SplitTest\Sessions\CISession;
use FullCycle\SplitTest\Sessions\IlluminateSession;
use Illuminate\Session\Store as SessionStore;
use \Log;
use Detection\MobileDetect;
use Jenssegers\Agent\Agent;


if (!function_exists("log_message")) {  // Kludge for making this logging work with laravel.  Should do this in manager.
    function log_message($level,$message) {
        Log::$level($message);
    }
}

class SplitTest {
    
    protected $exemptRoutes = [
        "/esp_api_v2/*",
        "/robots.txt",
        "/xml_feeds/*",
        "/ads.txt",
    ];

	function __construct($session_store, $manager) {
	    if ($session_store instanceof SessionStore)
	       $this->session = new IlluminateSession($session_store);	  // This needs to be dynamic.
		else 
		    $this->session = new CISession($session_store);	  // This needs to be dynamic.
		$this->manager = $manager;                            // Model is more of a manager.
		//$this->detect = new MobileDetect;
		$this->detect = new Agent;
		
	
	}
	

	// Not right,using ome CI syntax in here, that needs to be abstracted away.
	/*
	function getActiveRandomTest() {
        $result = $this->getActiveSplitTests();
        if (!$this->result)	// No split tests are active
            return null;
            
        $count = $result->num_rows();
        srand(time());
        $random = rand(0,$count);
        return $result->row($random);
	}
	*/
	
	function isRouteExempt() {
		// No request URI then make it exempt
	    if (!isset($_SERVER['REQUEST_URI']))
		return true;
	    $uri = $_SERVER['REQUEST_URI'];
	    foreach($this->exemptRoutes as $exemptRoute) {
	        if (fnmatch($exemptRoute,$uri)) 
	            return true;
	    }
	    return false;
	}
	
	function getSplitTestGroup($member_id) {
	    // Does member already have a split test assigned?
	    $force_split_test_id = $this->manager->getForceSplitTestId();
	    if ($force_split_test_id) {
	        $split_test = $this->manager->getSplitTest($force_split_test_id);
	        if ($split_test && ($split_test->status == "active" || $split_test->status == "testing" ))
	            return $split_test;
	    }
	    $member_split_test = $this->manager->getMemberSplitTest($member_id);
	    if ($member_split_test)
	        return $member_split_test;
	    $random = $this->manager->getActiveRandomTest();
	    return $random;
	}
	
	function getSessionId() {
	    return $this->session->get("sv_session_id");
	}
	
	function checkMemberIdentified($member_id, $split_id) {
	    if (!$member_id)   // No member identity to go on.. nothing to check.
	        return false;
	    $stored_member_id = $this->session->get("member_id");
	    if ($stored_member_id != $member_id) { // We have a new identity.
	        $sv_session_id = $this->getSessionId();
	        // $member_split_test = $this->manager->getMemberActiveSplitTest($member_id);
	        $member_split_test = $this->manager->getMemberSplitTest($member_id);
	        $this->manager->addMemberToSplitTest($split_id, $member_id, $sv_session_id);
	        // $this->manager->updateLineItem($sv_session_id,['user_id'=>$member_id]);
	        if ($member_split_test) {
	            //log_message("info","Member has active split test: " . print_r($member_split_test,true));
	            if ($split_id != $member_split_test->id) { // Lets update their session.
	                $this->manager->splitTestReplaced($member_split_test,$split_id);
	            }
	        }
	    }
	}
	
	function setUserSplit($member_id = false) {
	    if ($this->isRouteExempt())
	        return;
		$split_id = $this->session->get('split_id');
		// log_message("info","!!!!!!!!!!!!!! Split test id: $split_id");
		$force_split_test_id = $this->manager->getForceSplitTestId();
		if ($force_split_test_id && $force_split_test_id != $split_id) {
		    $split_id = false;
		    $this->session->forget();
		}
		if ($split_id) {
		      $splitTestGroup = $this->manager->getSplitTest($split_id);
		      if ($splitTestGroup->status == 'active' || $splitTestGroup->status == 'testing')  {// If active || testing then stay in this test.
		          $this->checkMemberIdentified($member_id,$split_id);
		          $this->addCustomerTrackingId();
			     return ;
		      } 
		      $this->session->forget();
		}
//		$active_tests = $this->manager->getActiveSplitTests();
		$splitTestGroup = $this->getSplitTestGroup($member_id);
		if ($splitTestGroup) {
		    $this->setSplitTestGroup($splitTestGroup, $member_id);
		}
	}
	
	function create_session_id() {
	    // $sv_session_id = md5(time().rand());
	    $sv_session_id = md5(microtime().rand());
	    
	    return $sv_session_id;
	}
	
	function getDeviceClass() {
	    if ($this->detect->isPhone())
	        return "phone";
	    if ($this->detect->isTablet())
	        return "tablet";
        if ($this->detect->isMobile())
            return "mobile";
	    if ($this->detect->isDesktop())
	        return "desktop";
        if ($this->detect->isRobot())
            return "bot";
	            
	    return "other";
	}
	
	function getDeviceOS() {
	    if ($this->detect->isRobot())
	        return $this->detect->robot()?:null;
        return $this->detect->platform()?:null;
	}
	
	function addLineItem($sv_session_id, $variant, $mobile = 0) {
	    $uri = isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:null;
	    $referer = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:null;
	    $useragent = isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:null;
	    
	    // Line to test user agents
	    // $this->detect->setUserAgent('Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)');
	    $is_mobile = $this->detect->isMobile();
	    $os = $this->getDeviceOS();
	    $device_class = $this->getDeviceClass();
	    $mobile_grade = $this->detect->mobileGrade();
	    $data = array(
	        'new_account' => false,
	        'mobile' => $mobile,
	        'variant' => $variant,
	        'revenue' => 0,
	        'session_id' => $sv_session_id,
	        'uri' => $uri,
	        'referer' => $referer,
	        'mobile' => $is_mobile,
	        'useragent' => $useragent,
	        'already_logged_in' => $this->session->getRaw('customer_is_logged')?1:0,
	        'device_class' => $this->getDeviceClass(),
	        'operating_system' => $os,
	        'mobile_grade' => $mobile_grade,
	        
	    );
	    $this->manager->insertLineItem($sv_session_id,$data);
	}
	
	function update($data) {
	    $this->manager->updateLineItem($this->getSessionId(),$data);
	}
	
	function addCustomerTrackingId() {
	    $st_customer_tracking_id = $this->session->get('customer_tracking_id');
	    $customer_tracking = $this->session->getRaw('customer_tracking');
	    if ($customer_tracking && $st_customer_tracking_id != $customer_tracking->id) {
	        $this->session->put('customer_tracking_id',$customer_tracking->id);
	        $this->update(['customer_tracking_id'=>$customer_tracking->id]);
	    }
	}
	
	function setSplitTestGroup($splitTestGroup, $member_id = false) {
	    //log_message("info","Setting split_id in session: {$splitTestGroup->id}");
	    $this->session->put("split_id",$splitTestGroup->id);
	    $this->session->put("meta",$this->manager->getMeta($splitTestGroup->id));
	    $this->session->put("entry",(array) $splitTestGroup);
	    $this->session->put("member_id",$member_id);
	    if (!empty($splitTestGroup->session_id)) {
	        $sv_session_id = $splitTestGroup->session_id;
	    } else {   // Only increment and add them if we were not able to identify them with an existing session
	       $sv_session_id = $this->create_session_id();
	       $this->manager->incrementUserCount($splitTestGroup->id);
	       $this->addLineItem($sv_session_id,$splitTestGroup->id);
	    }
	    $this->session->put('sv_session_id', $sv_session_id);
	    if ($member_id) {
	        $this->manager->addMemberToSplitTest($splitTestGroup->id, $member_id, $sv_session_id);
	    }
	    $this->addCustomerTrackingId();
	}
	
	function getMeta() {
	    $meta = $this->session->get('meta');
	    return $meta;
	}

	function get($key=false) {
		return $this->session->get($key);
	}
		
	function incrementCartAdd() {
	    return $this->manager->incrementCartAdd($this->getSessionId());
	}
	
	function incrementDealsViewed() {
	    return $this->manager->incrementDealsViewed($this->getSessionId());
	}
	
	function incrementLogins() {
	    return $this->manager->incrementLogins($this->getSessionId());
	}
	
	function incrementLogouts() {
	    return $this->manager->incrementLogouts($this->getSessionId());
	}
	
	function incrementRegistered() {
	    return $this->manager->incrementRegistered($this->getSessionId());
	}
	
	function addOrderInfo($order_id,$amount,$shipping,$tax,$qty) {
	    return $this->manager->addOrderInfo($this->getSessionId(),$order_id,$amount,$shipping,$tax,$qty);
	}
}



